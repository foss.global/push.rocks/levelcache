import { CacheEntry } from './levelcache.classes.cacheentry.js';

export abstract class AbstractCache {
  public abstract ready: Promise<void>;
  public abstract status: 'active' | 'inactive';

  // Cache Entries
  /**
   * store a Blob
   */
  public abstract storeCacheEntryByKey(keyArg: string, valueArg: CacheEntry): Promise<void>;

  /**
   * retrieve cache entry
   */
  public abstract retrieveCacheEntryByKey(keyArg: string): Promise<CacheEntry>;

  /**
   * checks for the presence of a key
   * @param keyArg
   */
  public abstract checkKeyPresence(keyArg: string): Promise<boolean>;

  /**
   * delete a key
   */
  public abstract deleteCacheEntryByKey(keyArg: string): Promise<void>;

  /**
   * clean the cache
   */
  public abstract cleanOutdated(): Promise<void>;

  /**
   * cleans the complete cache
   */
  public abstract cleanAll(): Promise<void>;
}
