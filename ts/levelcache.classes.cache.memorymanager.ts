import * as plugins from './levelcache.plugins.js';
import { AbstractCache } from './levelcache.abstract.classes.cache.js';
import { CacheEntry } from './levelcache.classes.cacheentry.js';
import { ILevelCacheConstructorOptions, LevelCache } from './levelcache.classes.levelcache.js';

export class CacheMemoryManager extends AbstractCache {
  private levelCacheRef: LevelCache;
  private fastMap = new plugins.lik.FastMap<CacheEntry>();
  private readyDeferred = plugins.smartpromise.defer<void>();

  public ready = this.readyDeferred.promise;
  public status: 'active' | 'inactive';

  constructor(levelCacheRefArg: LevelCache) {
    super();
    this.levelCacheRef = levelCacheRefArg;
    this.init();
  }

  public async init() {
    this.status = 'active';
    this.readyDeferred.resolve();
  }

  public async storeCacheEntryByKey(keyArg: string, cacheEntryArg: CacheEntry): Promise<void> {
    this.fastMap.addToMap(keyArg, cacheEntryArg, { force: true });
  }

  public async retrieveCacheEntryByKey(keyArg: string): Promise<CacheEntry> {
    return this.fastMap.getByKey(keyArg);
  }

  public async checkKeyPresence(keyArg: string): Promise<boolean> {
    if (this.fastMap.getByKey(keyArg)) {
      return true;
    } else {
      return false;
    }
  }

  public async deleteCacheEntryByKey(keyArg: string) {
    this.fastMap.removeFromMap(keyArg);
  }

  public async cleanOutdated() {}

  public async cleanAll() {
    this.fastMap.clean();
  }
}
