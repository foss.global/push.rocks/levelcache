/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/levelcache',
  version: '3.0.3',
  description: 'a cache that uses memory/disk/s3 as backup'
}
