import * as plugins from './levelcache.plugins.js';

export const packageDir = plugins.path.join(
  plugins.smartpath.get.dirnameFromImportMetaUrl(import.meta.url),
  '../'
);
export const nogitDir = plugins.path.join(packageDir, '.nogit/');
