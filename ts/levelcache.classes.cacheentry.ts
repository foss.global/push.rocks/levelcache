import * as plugins from './levelcache.plugins.js';

export interface ICacheEntryConstructorOptions {
  key?: string;
  ttl: number;
  typeInfo?: string;
  contents: Buffer;
}

/**
 * a CacheEntry
 */
export class CacheEntry
  extends plugins.smartjson.Smartjson
  implements ICacheEntryConstructorOptions
{
  public static fromStorageJsonString(storageJsonString: string) {
    return new CacheEntry(plugins.smartjson.parse(storageJsonString));
  }

  @plugins.smartjson.foldDec()
  public key: string;

  @plugins.smartjson.foldDec()
  public ttl: number;

  @plugins.smartjson.foldDec()
  public createdAt: number;

  @plugins.smartjson.foldDec()
  public typeInfo: string;

  @plugins.smartjson.foldDec()
  contents: Buffer;

  public toStorageJsonString(): string {
    return this.foldToJson();
  }

  constructor(optionsArg: ICacheEntryConstructorOptions) {
    super();
    Object.assign(this, optionsArg);
  }
}
