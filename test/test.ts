import { expect, tap } from '@pushrocks/tapbundle';
import * as levelcache from '../ts/index.js';
import { CacheEntry } from '../ts/index.js';

let testLevelCache: levelcache.LevelCache;

tap.test('should create a new levelcache instance', async () => {
  testLevelCache = new levelcache.LevelCache({
    cacheId: 'myCache',
  });
  expect(testLevelCache).toBeInstanceOf(levelcache.LevelCache);
});

tap.test('should cache a value', async () => {
  await testLevelCache.storeCacheEntryByKey(
    'mykey',
    new CacheEntry({
      contents: Buffer.from('heythere'),
      ttl: 10000,
      typeInfo: 'string',
    })
  );
  const result = await testLevelCache.retrieveCacheEntryByKey('mykey');
  expect(result.contents.toString()).toEqual('heythere');
});

tap.test('should respect ttl', async (tools) => {
  await testLevelCache.storeCacheEntryByKey(
    'mykey',
    new CacheEntry({
      contents: Buffer.from('heythere'),
      ttl: 1000,
      typeInfo: 'string',
    })
  );
  const result = await testLevelCache.retrieveCacheEntryByKey('mykey');
  expect(result.contents.toString()).toEqual('heythere');
  await tools.delayFor(1100);
  const result2 = await testLevelCache.retrieveCacheEntryByKey('mykey');
  expect(result2).toBeNull();
});

tap.start();
